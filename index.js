import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View,
  TextInput,
  Platform,
  Dimensions,
  Image
} from 'react-native';
import {connect} from 'react-redux';
import MyIcon from 'react-native-vector-icons/Ionicons';
import ScalableText from 'react-native-text';

var WIDTH= Dimensions.get('window').width;
var HEIGHT = Dimensions.get('window').height;


const mapStateToProps = (state) => {
  return {

  }
}


const mapDispatchToProps = (dispatch) => {
  return {}
}


class ChooseFilter extends React.Component {

  static defaultProps={
    containerStyle: {
      borderBottomWidth:0,
      borderBottomColor:'white',
      height:HEIGHT*.12
    },
    filterTypeStyle:{
      fontSize:18,
      fontFamily:'PingFang SC',
      color:'black'
    },
    filterTypeData:'',
    filterSubTypeStyle:{
      fontSize:12,
      fontFamily:'PingFang SC',
      color:'#4a4a4a'
    },
    filterSubTypeData:'',
    filterValueStyle:{
      fontSize:14,
      fontFamily:'PingFang SC',
      color:'#ababab'
    },
    filterValue:'',
    filterIconName:'',
    filterIconStyle:{
      fontSize:30,
      color:'#d8d8d8',
      marginHorizontal:5
    },
    action:()=>{},
    isLogo:false,
    valueLogoStyle:{
      height:WIDTH*.1,
      width:WIDTH*.1,
      borderRadius:(WIDTH*.1)/2,
      borderWidth:1,
      borderColor:'#d8d8d8',
      backgroundColor:'white'
    },
    valueImage:'',
    valueImageStyle:{
      height:WIDTH*.1,
      width:WIDTH*.1,
      borderRadius:(WIDTH*.1)/2,
    },
    navigateTo:'',
    filterIconViewStyle:{
      justifyContent:'center',
      alignItems:'flex-end'
    }
  }

  constructor(props) {
    super(props);
  }

  componentDidMount(){
  }

  componentWillMount(){
  }

  render(){
    let {
      containerStyle,
      filterTypeStyle,
      filterTypeData,
      filterSubTypeStyle,
      filterSubTypeData,
      filterValueStyle,
      filterValue,
      filterIconName,
      filterIconStyle,
      lastVal,
      action,
      isLogo,
      valueLogoStyle,
      valueImage,
      valueImageStyle,
      navigateTo,
      filterIconViewStyle
    }=this.props;
return(
  <TouchableOpacity activeOpacity={(navigateTo?2:1)} style={[{flex:.15,flexDirection:'row'},containerStyle]}
    onPress={()=>{
      action();
    }}
  >
    {filterSubTypeData ? (
      <View style={{flex:2}}>
        <View style={{flex:2,justifyContent:'flex-end',paddingBottom:5}}>
          <ScalableText style={filterTypeStyle}>{filterTypeData}</ScalableText>
        </View>
        <View style={{flex:1}}><ScalableText  style={filterSubTypeStyle}>{filterSubTypeData}</ScalableText>
        </View>
      </View>
    ) : (
      <View style={{flex:2.5,justifyContent:'center'}}>
        <ScalableText style={filterTypeStyle}>{filterTypeData}</ScalableText>
      </View>
    )}
    {isLogo ? (
      <View style={{flex:3,justifyContent:'center',alignItems:'flex-end'}}>
        <View style={valueLogoStyle}>
        <Image
          style={valueImageStyle}
          source={{uri:valueImage}}
          resizeMode={'cover'}
         >
        </Image>
        </View>
      </View>
    ) : (
      <View style={{flex:3,justifyContent:'center',alignItems:'flex-end'}}>
        <ScalableText  style={filterValueStyle}>{filterValue}</ScalableText>
      </View>
    )}

    <View style={[{flex:.8},filterIconViewStyle]}>
      <MyIcon name={filterIconName} style={filterIconStyle}/>
    </View>
  </TouchableOpacity>
)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChooseFilter)
